#### POST entity
```bash
curl -X "POST" "http://localhost:1026/v2/entities?options=keyValues" \
  -H 'Content-Type:application/json' \
  -d $'{
      "id": "vehicle:WasteManagement:1",
      "type": "Vehicle",
      "category": {
          "value": ["municipalServices"]
      },
      "vehicleType": {
          "value": "lorry"
      },
      "name": {
          "value": "C Recogida 1"
      },
      "vehiclePlateIdentifier": {
          "value": "3456ABC"
      },
      "refVehicleModel": {
          "type": "Relationship",
          "value": "vehiclemodel:econic"
      },
      "location": {
          "type": "geo:json",
          "value": {
              "type": "Point",
              "coordinates": [-3.164485591715449, 40.62785133667262]
          },
          "metadata": {
              "timestamp": {
                  "type": "DateTime",
                  "value": "2018-09-27T12:00:00"
              }
          }
      },
      "battery": {
        "value": 4.125,
        "metadata": {
          "timestamp": {
            "type": "DateTime",
            "value": "2018-09-27T12:00:00"
          }
        }
      },
      "speed": {
          "value": 50,
          "metadata": {
              "timestamp": {
                  "type": "DateTime",
                  "value": "2018-09-27T12:00:00"
              }
          }
      },
      "serviceProvided": {
          "value": ["garbageCollection", "wasteContainerCleaning"]
      }
  }'
```

#### GET entity
```bash
curl "http://localhost:1026/v2/entities/vehicle:WasteManagement:1/attrs?options=keyValues" \
  -H 'Accept: application/json'
```

#### Update entity
```bash
curl -X "PATCH" "http://localhost:1026/v2/entities/vehicle:WasteManagement:1/attrs?options=keyValues" \
  -H 'Content-Type: application/json' \
  -d $'{
    "location": {
      "type": "geo:json",
      "value": {
        "type": "Point",
        "coordinates": [-10, 10]
      },
      "metadata": {
        "timestamp": {
        "type": "DateTime",
        "value": "2019-01-11T12:00:10"
        }
      }
    }
  }'
```

```bash
curl -X "PATCH" "http://localhost:1026/v2/entities/vehicle:WasteManagement:1/attrs?options=keyValues" \
  -H 'Content-Type: application/json' \
  -d $'{
    "battery": {
      "value": 5,
      "metadata": {
        "timestamp": {
        "type": "DateTime",
        "value": "2019-01-11T12:00:10"
        }
      }
    }
  }'
```

#### subscribe to updates
##### Location
```bash
curl -X "POST" "http://localhost:1026/v2/subscriptions" \
  -H 'Content-Type: application/json' \
  -d $'{
    "subject": {
      "condition": {
        "attrs": [
          "location"
        ]
      },
      "entities": [
        {
          "type": "Vehicle",
          "idPattern": ".*"
        }
      ]
    },
    "description": "A subscription to location data",
    "notification": {
      "attrs": [
        "location"
      ],
      "http": {
        "url": "http://cygnus-ngsi.docker:5050/notify"
      },
      "attrsFormat": "legacy"
    },
    "expires": "2020-04-20T14:00:00.00Z"
  }'
```

##### Speed
```bash
curl -X "POST" "http://localhost:1026/v2/subscriptions" \
  -H 'Content-Type: application/json' \
  -d $'{
    "subject": {
      "condition": {
        "attrs": [
          "speed"
        ]
      },
      "entities": [
        {
          "type": "Vehicle",
          "idPattern": ".*"
        }
      ]
    },
    "description": "A subscription to speed data",
    "notification": {
      "attrs": [
        "speed"
      ],
      "http": {
        "url": "http://cygnus-ngsi.docker:5050/notify"
      },
      "attrsFormat": "legacy"
    },
    "expires": "2020-04-20T14:00:00.00Z"
  }'
```

##### Battery
```bash
curl -X "POST" "http://localhost:1026/v2/subscriptions" \
  -H 'Content-Type: application/json' \
  -d $'{
    "subject": {
      "condition": {
        "attrs": [
          "battery"
        ]
      },
      "entities": [
        {
          "type": "Vehicle",
          "idPattern": ".*"
        }
      ]
    },
    "description": "A subscription to battery data",
    "notification": {
      "attrs": [
        "battery"
      ],
      "http": {
        "url": "http://cygnus-ngsi.docker:5050/notify"
      },
      "attrsFormat": "legacy"
    },
    "expires": "2020-04-20T14:00:00.00Z"
  }'
```

#### GET subscriptions
```bash
curl "http://localhost:1026/v2/subscriptions"
```

#### DELETE Subscription
```bash
curl -X "DELETE" "http://localhost:1026/v2/subscriptions/{id}"
```

#### GET updates from subscription
```bash
curl "http://localhost:8080/v2/entities/vehicle:WasteManagement:1/attrs/location?time=2018-10-10&timerel=between&offset=0&limit=100&endtime=2020-12-31&type=Vehicle"
```

#### GET list of Vehicles
```bash
curl "http://localhost:1026/v2/entities?type=Vehicle&attrs=id&options=keyValues"
```
