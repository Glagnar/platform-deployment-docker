# Introduction
This project represents the reference platform implementation of the EU H2020 [SynchroniCity](https://synchronicity-iot.eu/) project.
In order to make the deployment as easy and quick as possible, the platform architecure was defined with a container deployment approach using [Docker compose](https://docs.docker.com/compose/).

For more information follow the guide [here](https://docs.synchronicity-iot.eu).