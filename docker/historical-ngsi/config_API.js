var config = {};

//== Configured for Cygnus on Test Environment =================================
config.dbPort = 27017;
config.dbAddr = 'mongo-his.docker';

//== Cygnus/SynchroniCity Historical Mongo Database 
config.dbName = 'HIS_HIS'; 


//== Environment ===============================================================
config.env = 'debug'; // 'production'

//== Historical API Ports ======================================================
config.httpPort = 8080;
config.httpsPort = 8090;

//== Bug level =================================================================
config.logLevel = 'debug'; // { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }

module.exports = config;
 